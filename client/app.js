var ENTER_KEY_CODE = 13;

var username = null;
var connection = null;

function init(event) {
    if ( event.keyCode == ENTER_KEY_CODE ) {
        setDisplayValueOnEach(".not-logged-in", "none");
        setDisplayValueOnEach(".logged-in", "block");

        username = this.value;
        document.querySelector("#name").innerHTML = username;

        connection = new WebSocket("ws://127.0.0.1:1337/ws.yaws");
        connection.onopen = onSocketOpen;
        connection.onclose = onSocketClose;
        connection.onmessage = onSocketReceiveMessage;
        connection.onerror = onSocketError;
    }
}

function setDisplayValueOnEach(classname, value) {
    [].forEach.call(document.querySelectorAll(classname), function(item) {
        item.style.display = value;
    });
}

function onSocketOpen() {
    var input = document.querySelector("#compose input");
    input.onkeypress = sendMessage;
}

function onSocketClose(event) {
    setDisplayValueOnEach(".not-logged-in", "block");
    setDisplayValueOnEach(".logged-in", "none");
    onSocketError({message: "Server just had closed your connection."});
}

function onSocketReceiveMessage(event) {
    var message = JSON.parse(event.data);
    printMessage(message.author, message.text);
}

function onSocketError(error) {
    var errorArea = document.querySelector("#error");
    errorArea.style.display = "block";
    errorArea.innerHTML = error.message;
}

function sendMessage(event) {
    if ( event.keyCode === ENTER_KEY_CODE ) {
        var message = {author: username, text: this.value};
        connection.send(JSON.stringify(message));

        this.value = "";
    }
}

function printMessage(from, text) {
    var template = document.querySelector("#message-template");
    var message = template.cloneNode(true);

    message.removeAttribute("id");
    message.querySelector(".from").innerHTML = from;
    message.querySelector(".text").innerHTML = text;

    var messageArea = document.querySelector("#messages");
    messageArea.insertBefore(message, messageArea.firstChild);
}

window.onload = function() {
    var input = document.querySelector("#header input");
    input.onkeypress = init;
};
