-module(chat).
-export([init/1]).
-export([handle_message/1]).
-export([handle_info/2]).

-define(KEY, {p, l, broadcast}).

init([_, InitialState]) ->
    application:ensure_started(gproc),
    gproc:reg(?KEY),
    {ok, InitialState}.

handle_message({text, Message}) ->
    gproc:send(?KEY, Message),
    noreply;

handle_message({close, Status, _}) ->
    {close, Status}.

handle_info(Info, State) ->
    yaws_api:websocket_send(self(), {text, <<Info/binary>>}),
    {noreply, State}.
