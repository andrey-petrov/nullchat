# nullchat

A tiny, dump and featureless chat application on top of Erlang and Yaws.

### Install

Ensure you do have "yaws" and "gproc" erlang libraries installed on your system and clone the repository.

### Run

Open your favorite terminal emulator and navigate to the directory you have just cloned, run `make` to compile the source and then `./run.sh` to start a web server. If everything is fine, point your web browser to `http://127.0.0.1/1337` and join the party.

### Terminate 

Simply type `q().` in the erlang console to stop Yaws from keeping port 1337 busy.